// TicTacToe JS functions
// 16.1.2023 corrected a bug leading to alter winner name after a win

var row1 = [0, 0, 0];   // 0 = grass, 1 = hero, 2 = dragon
var row2 = [0, 0, 0];
var row3 = [0, 0, 0];
var turn = 1, winStat = 0;           //1 = hero, 2 = draqon

function newGame() {
    
   // Clear the visible area
    for (let a = 0; a <= 2; a++)
    {
        for (let b = 0; b <= 2; b++)
        {
            document.getElementById("ruutu" + a + "." + b ).src = "grass.png";
        }
    }
    
    // This clears the table
    row1 = [0,0,0];
    row2 = [0,0,0];
    row3 = [0,0,0];
    
    turn = 1;
    winStat = 0;
    
    document.getElementById("status").innerHTML = "New game started!<br>Hero's turn.";
    
    return 0;
    }
    
function placeItem(y, x) {
   
   if(winStat == 0) {
      // Check if there is item in location
      switch(y) {
         case 0:
            if(row1[x] != 0) {
               document.getElementById("status").innerHTML = "Cannot place item there!";
               return 0;
            }
            break;
         case 1:
            if(row2[x] != 0) {
               document.getElementById("status").innerHTML = "Cannot place item there!";
               return 0;
            }
            break;
         case 2:
            if(row3[x] != 0) {
               document.getElementById("status").innerHTML = "Cannot place item there!";
               return 0;
            }
            break;
         }
    
      // Put the item in table
    switch(y) {
       case 0:
          row1[x] = turn;
          break;
       case 1:
          row2[x] = turn;
          break;
       case 2:
          row3[x] = turn;
          break;
       }
     
     // Put the item on screen
    if(turn == 1) {
       document.getElementById("ruutu" + y + "." + x ).src = "grass_hero.png";
       document.getElementById("status").innerHTML = "Dragon's turn!";
    } else {
       document.getElementById("ruutu" + y + "." + x ).src = "grass_dragon.png";
       document.getElementById("status").innerHTML = "Hero's turn!";
    }
   }
   
   
   // Check vitcory
   if (victoryStatus() != 0) {
      if( turn == 1 ) {
         document.getElementById("status").innerHTML = "HERO IS WINNER!<br>Start new game.";
      } else {
         document.getElementById("status").innerHTML = "DRAGON IS WINNER!<br>Start new game.";
      }
      winStat = 1;
   }
   
   //double if-s because checking also win status for "status".innerHTML to work right after win
   if ( turn == 1 && winStat == 0) {
      turn = 2;
   } else if ( turn == 2 && winStat == 0) {
      turn = 1;
   }
   
   return 0;
}

function victoryStatus() {
   
   // this checks victory in x axis
   for (i=1; i <= 2; i++) {
      if(row1[0] == i && row1[1] == i && row1[2] == i) {
         return 1;
      } else if(row2[0] == i && row2[1] == i && row2[2] == i) {
         return 1;
      }  else if(row3[0] == i && row3[1] == i && row3[2] == i) {
         return 1;
      }
   }
   
   //this checks victory in y axis
   for (i=1; i <= 2; i++) {
      if(row1[0] == i && row2[0] == i && row3[0] == i) {
         return 1;
      } else if(row1[1] == i && row2[1] == i && row3[1] == i) {
         return 1;
      }  else if(row1[2] == i && row2[2] == i && row3[2] == i) {
         return 1;
      }
   }
   
   //this checks victory in diagonally
   for (i=1; i <= 2; i++) {
      if(row1[0] == i && row2[1] == i && row3[2] == i) {
         return 1;
      } else if(row1[2] == i && row2[1] == i && row3[0] == i) {
         return 1;
      } 
   }
   
   
   return 0;
}